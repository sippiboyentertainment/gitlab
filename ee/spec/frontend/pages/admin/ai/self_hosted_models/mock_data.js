export const mockCustomModel = {
  id: 1,
  name: 'mock-custom-model-name',
  model: 'mock-custom-model-type',
  api_token: '',
  endpoint: 'https://mock-endpoint.com',
};
